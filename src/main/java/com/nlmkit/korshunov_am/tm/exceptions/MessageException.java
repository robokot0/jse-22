package com.nlmkit.korshunov_am.tm.exceptions;

public class MessageException extends Exception {
    /**
     * Конструктор
     * @param message Сообщение об ошибке
     */
    public MessageException(String message) {
        super(message);
    }

}
