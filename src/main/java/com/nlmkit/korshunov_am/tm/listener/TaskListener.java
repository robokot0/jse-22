package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.service.TaskService;

import java.io.IOException;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.TerminalConst.HELP;

public class TaskListener extends AbstractListener implements Listener {
    /**
     * Сервис задач
     */
    TaskService taskService = TaskService.getInstance();
    /**
     * Приватный конструктор по умолчанию
     */
    private TaskListener(){
        super();
    }
    /**
     * Единственный экземпляр объекта TaskListener
     */
    private static TaskListener instance = null;

    /**
     * Получить единственный экземпляр объекта TaskListener
     * @return единственный экземпляр объекта TaskListener
     */
    public static TaskListener getInstance(){
        if (instance == null){
            instance = new TaskListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_TASK_CREATE:
                case TASK_CREATE:return taskService.createTask();
                case SHORT_TASK_CLEAR:
                case TASK_CLEAR:return taskService.clearTask();
                case SHORT_TASK_LIST:
                case TASK_LIST:return taskService.listTask();
                case SHORT_TASK_VIEW:
                case TASK_VIEW:return taskService.viewTaskByIndex();
                case SHORT_TASK_REMOVE_BY_ID:
                case TASK_REMOVE_BY_ID:return taskService.removeTaskByID();
                case SHORT_TASK_REMOVE_BY_NAME:
                case TASK_REMOVE_BY_NAME:return taskService.removeTaskByName();
                case SHORT_TASK_REMOVE_BY_INDEX:
                case TASK_REMOVE_BY_INDEX:return taskService.removeTaskByIndex();
                case SHORT_TASK_UPDATE_BY_INDEX:
                case TASK_UPDATE_BY_INDEX:return taskService.updateTaskByIndex();
                case SHORT_TASK_ADD_TO_PROJECT_BY_IDS:
                case TASK_ADD_TO_PROJECT_BY_IDS:return taskService.addTaskToProjectByIds();
                case SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS:
                case TASK_REMOVE_FROM_PROJECT_BY_IDS:return taskService.removeTaskFromProjectByIds();
                case SHORT_TASK_LISTS_BY_PROJECT_ID:
                case TASK_LISTS_BY_PROJECT_ID:return taskService.listTaskByProjectId();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return taskService.saveAsJson();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return taskService.saveAsXML();
                case SHORT_HELP:
                case HELP: return taskService.displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            commandHistoryService.ShowResult("[FAIL] "+e.getMessage());
        }
        return 0;
    }
}
