package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.service.SystemService;


import java.io.IOException;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;

public class SystemListener extends AbstractListener implements Listener {
    /**
     * Сервис системных комманд
     */
    SystemService systemService = SystemService.getInstance();
    /**
     * Приватный конструктор по умолчанию
     */
    private SystemListener(){
        super();
        systemService.displayWelcome();
    }
    /**
     * Единственный экземпляр объекта SystemListener
     */
    private static SystemListener instance = null;

    /**
     * Получить единственный экземпляр объекта SystemListener
     * @return единственный экземпляр объекта SystemListener
     */
    public static SystemListener getInstance(){
        if (instance == null){
            instance = new SystemListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_VERSION:
                case VERSION: return systemService.displayVersion();
                case SHORT_ABOUT:
                case ABOUT:return systemService.displayAbout();
                case SHORT_HELP:
                case HELP:return systemService.displayHelp();
                case SHORT_EXIT:
                case EXIT:return systemService.displayExit();
                case SHORT_LOAD_TEST_DATA:
                case LOAD_TEST_DATA:return systemService.loadTestDataAndShowResult();
                default:return -1;
            }
        }
        catch (MessageException e) {
            commandHistoryService.ShowResult("[FAIL] "+e.getMessage());
        }
        return 0;
    }
}
