package com.nlmkit.korshunov_am.tm.listener;

import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.service.UserService;

import java.io.IOException;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.TerminalConst.HELP;

public class UserListener extends AbstractListener implements Listener {
    /**
     * Сервис задач
     */
    UserService userService = UserService.getInstance();
    /**
     * Приватный конструктор по умолчанию
     */
    private UserListener(){
        super();
    }
    /**
     * Единственный экземпляр объекта UserListener
     */
    private static UserListener instance = null;

    /**
     * Получить единственный экземпляр объекта UserListener
     * @return единственный экземпляр объекта UserListener
     */
    public static UserListener getInstance(){
        if (instance == null){
            instance = new UserListener();
        }
        return instance;
    }



    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_USER_CREATE:
                case USER_CREATE:return userService.createUser();
                case SHORT_USER_LIST:
                case USER_LIST:return userService.listUser();
                case SHORT_USER_VIEW_BY_ID:
                case USER_VIEW_BY_ID:return userService.viewUserById();
                case SHORT_USER_VIEW_BY_INDEX:
                case USER_VIEW_BY_INDEX:return userService.viewUserByIndex();
                case SHORT_USER_VIEW_BY_LOGIN:
                case USER_VIEW_BY_LOGIN:return userService.viewUserByLogin();
                case SHORT_USER_REMOVE_BY_ID:
                case USER_REMOVE_BY_ID:return userService.removeUserById();
                case SHORT_USER_REMOVE_BY_INDEX:
                case USER_REMOVE_BY_INDEX:return userService.removeUserByIndex();
                case SHORT_USER_REMOVE_BY_LOGIN:
                case USER_REMOVE_BY_LOGIN:return userService.removeUserByLogin();
                case SHORT_USER_UPDATE_BY_ID:
                case USER_UPDATE_BY_ID:return userService.updateUserDataById();
                case SHORT_USER_UPDATE_BY_INDEX:
                case USER_UPDATE_BY_INDEX:return userService.updateUserDataByIndex();
                case SHORT_USER_UPDATE_BY_LOGIN:
                case USER_UPDATE_BY_LOGIN:return userService.updateUserDataByLogin();
                case SHORT_USER_UPDATE_PASSWORD_BY_ID:
                case USER_UPDATE_PASSWORD_BY_ID:return userService.updateUserPasswordById();
                case SHORT_USER_UPDATE_PASSWORD_BY_INDEX:
                case USER_UPDATE_PASSWORD_BY_INDEX:return userService.updateUserPasswordByIndex();
                case SHORT_USER_UPDATE_PASSWORD_BY_LOGIN:
                case USER_UPDATE_PASSWORD_BY_LOGIN:return userService.updateUserPasswordByLogin();
                case SHORT_USER_AUTH:
                case USER_AUTH:return userService.authUser();
                case SHORT_USER_UPDATE_PASSWORD:
                case USER_UPDATE_PASSWORD:return userService.updateAuthUserPassword();
                case SHORT_USER_VIEW:
                case USER_VIEW:return userService.viewAuthUser();
                case SHORT_USER_UPDATE:
                case USER_UPDATE:return userService.updateAuthUser();
                case SHORT_USER_END:
                case USER_END:return userService.endAuthUserSession();
                case SHORT_USER_OF_TASK_SET_BY_INDEX:
                case USER_OF_TASK_SET_BY_INDEX:return userService.setTaskUserByIndex();
                case SHORT_USER_OF_PROJECT_SET_BY_INDEX:
                case USER_OF_PROJECT_SET_BY_INDEX:return userService.setProjectUserByIndex();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return userService.saveAsJSON();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return userService.saveAsXML();
                case SHORT_HELP:
                case HELP: return userService.displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            commandHistoryService.ShowResult("[FAIL] "+e.getMessage());
        }
        return 0;
    }
}
