package com.nlmkit.korshunov_am.tm.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.service.ProjectService;

import java.io.FileNotFoundException;
import java.io.IOException;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;
import static com.nlmkit.korshunov_am.tm.TerminalConst.HELP;

public class ProjectListener  extends AbstractListener implements Listener {
    ProjectService projectService = ProjectService.getInstance();
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectListener(){
        super();
    }
    /**
     * Единственный экземпляр объекта ProjectListener
     */
    private static ProjectListener instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectListener
     * @return единственный экземпляр объекта ProjectListener
     */
    public static ProjectListener getInstance(){
        if (instance == null){
            instance = new ProjectListener();
        }
        return instance;
    }

    @Override
    public int notify(String command) {
        try {
            switch (command) {
                case SHORT_PROJECT_CREATE:
                case PROJECT_CREATE: return projectService.createProject();
                case SHORT_PROJECT_CLEAR:
                case PROJECT_CLEAR: return projectService.clearProject();
                case SHORT_PROJECT_LIST:
                case PROJECT_LIST: return projectService.listProject();
                case SHORT_PROJECT_VIEW:
                case PROJECT_VIEW: return projectService.viewProjectByIndex();
                case SHORT_PROJECT_REMOVE_BY_ID:
                case PROJECT_REMOVE_BY_ID: return projectService.removeProjectByID();
                case SHORT_PROJECT_REMOVE_BY_NAME:
                case PROJECT_REMOVE_BY_NAME: return projectService.removeProjectByName();
                case SHORT_PROJECT_REMOVE_BY_INDEX:
                case PROJECT_REMOVE_BY_INDEX: return projectService.removeProjectByIndex();
                case SHORT_PROJECT_UPDATE_BY_INDEX:
                case PROJECT_UPDATE_BY_INDEX: return projectService.updateProjectByIndex();
                case SHORT_SAVE_TO_XML:
                case SAVE_TO_XML: return projectService.saveAsXML();
                case SHORT_SAVE_TO_JSON:
                case SAVE_TO_JSON: return projectService.saveAsJson();
                case SHORT_HELP:
                case HELP: return projectService.displayHelp();
                default:return -1;
            }
        }
        catch (MessageException | IOException e) {
            commandHistoryService.ShowResult("[FAIL] "+e.getMessage());
            }
        return 0;
    }
}
