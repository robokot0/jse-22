package com.nlmkit.korshunov_am.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    /**
     * Приватный конструктор по умолчанию
     */
    private UserRepository(){
    }
    /**
     * Единственный экземпляр объекта UserRepository
     */
    private static UserRepository instance = null;

    /**
     * Получить единственный экземпляр объекта UserRepository
     * @return единственный экземпляр объекта UserRepository
     */
    public static UserRepository getInstance(){
        if (instance == null){
            instance = new UserRepository();
        }
        return instance;
    }

    private List<User> users = new ArrayList<>();

    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public User create(final String login) {
        final User user = new User(login);
        users.add(user);
        return user;
    }
    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setPasswordHash(passwordHash);
        users.add(user);
        return user;
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        final User user = findById(id);
        user.setLogin(login);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setPasswordHash(passwordHash);
        return user;
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public User updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        final User user = findById(id);
        user.setLogin(login);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        return user;
    }
    /**
     * Изменить пароль пользователя по идентификатору
     * @param id идентификатор
     * @param passwordHash хэш пароля
     * @return пользователь
     */
    public User updatePasswordHash(final Long id, String passwordHash) {
        final User user = findById(id);
        user.setPasswordHash(passwordHash);
        return user;
    }
    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        users.clear();
    }
    /**
     * Найти пользователя по идентификатору.
     * @param id идентификатор
     * @return пользователь
     */
    public User findById(final Long id){
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }
    /**
     * Найти пользователя по индексу.
     * @param index Индекс
     * @return пользователь
     */
    public User findByIndex(final int index){
        return  users.get(index);
    }
    /**
     * Найти пользователя по login.
     * @param login
     * @return пользователь
     */
    public User findByLogin(final String login){
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }
    /**
     * Удалить пользователя по идентификатору
     * @param id идентифкатор
     * @return пользователь
     */
    public User removeById(final Long id){
        final User user = findById(id);
        users.remove(user);
        return user;
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return users;
    }

    /**
     * Получить количество пользователей
     * @return количество пользователей
     */
    public int size() {
        return users.size();
    }

    public void saveAsJSON() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream(this.getClass().getName()+".json"),this.users);
    }
    public void saveAsXML() throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.setVisibility(PropertyAccessor.FIELD,JsonAutoDetect.Visibility.ANY);
        mapper.writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream(this.getClass().getName()+".xml"),this.users);
    }

}
