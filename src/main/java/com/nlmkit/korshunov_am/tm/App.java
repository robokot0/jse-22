package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.entity.*;
import com.nlmkit.korshunov_am.tm.enumerated.*;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.listener.*;
import com.nlmkit.korshunov_am.tm.publisher.*;
import com.nlmkit.korshunov_am.tm.repository.*;
import com.nlmkit.korshunov_am.tm.service.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *  Тестовое приложение
 */
public class App {
    /**
     * Логгер
     */
    final static Logger logger = LogManager.getLogger(App.class.getName());
    /**
     * Источник комманд
     */
    static Publisher publisher = new PublisherImpl()
            .addListener(SystemListener.getInstance())
            .addListener(CommandHistoryListener.getInstance())
            .addListener(ProjectListener.getInstance())
            .addListener(TaskListener.getInstance())
            .addListener(UserListener.getInstance());
    /**
     * Точка входа
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) {
        if (args != null)
            if (args.length >=1) {
                final String param = args[0];
                publisher.notifyListener(param);
            }
        publisher.readAndExecuteCommand();
    }

}
