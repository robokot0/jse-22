package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

public class SystemService extends AbstractService  {
    /**
     * Приватный конструктор по умолчанию
     */
    private SystemService(){
        super(CommandHistoryService.getInstance());
    }
    /**
     * Единственный экземпляр объекта AbstractService
     */
    private static SystemService instance = null;

    /**
     * Получить единственный экземпляр объекта AbstractService
     * @return единственный экземпляр объекта AbstractService
     */
    public static SystemService getInstance(){
        if (instance == null){
            instance = new SystemService();
        }
        return instance;
    }

    /**
     * Показать выход
     * @return 0 выполнено
     */
    public int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }

    /**
     * Приглашение при входе в программу
     */
    public void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        System.out.println("Enter command");
    }

    /**
     * Показать версию
     * @return 0 выполнено
     */
    public int displayVersion() {
        System.out.println("1.0.0");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("Short command name in brackets");
        logger.info("----Common commands:");
        logger.info("version - Display version (v)");
        logger.info("about - Display developer info (a)");
        logger.info("help - Display list of command (h)");
        logger.info("exit - Terminate console application (e)");
        logger.info("exit - Terminate console application (e)");
        logger.info("save-to-json - Save data to JSON (sj)");
        logger.info("save-to-xml - Save data to XML (sx)");
        logger.info("load-test-data - Load test data (ltd)");
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать информацию о разработчике
     * @return
     */
    public int displayAbout() {
        logger.info("Andrey Korshunov");
        logger.info("korshunov_am@nlmk.com");
        ShowResult("[OK]");
        return 0;
    }
    public int loadTestDataAndShowResult() throws ProjectNotFoundException, WrongArgumentException, TaskNotFoundException {
        loadTestData();
        ShowResult("[OK]");
        return 0;
    }
    public void loadTestData() throws ProjectNotFoundException, WrongArgumentException, TaskNotFoundException {
        final ProjectRepository projectRepository=ProjectRepository.getInstance();
        final UserRepository userRepository=UserRepository.getInstance();
        final TaskRepostory taskRepostory=TaskRepostory.getInstance();

        projectRepository.clear();
        userRepository.clear();
        taskRepostory.clear();

        final ProjectTaskService projectTaskService=ProjectTaskService.getInstance();
        final UserService userService=UserService.getInstance();

        final User user1=userRepository.create("USER", Role.valueOf("USER"),"FN","SN","MN",userService.getStringHash("123"));
        final User user2=userRepository.create("USER1",Role.valueOf("USER"),"FN1","SN1","MN1",userService.getStringHash("123"));
        final User admin=userRepository.create("ADMIN",Role.valueOf("ADMIN"),"FN","SN","MN",userService.getStringHash("123"));

        final Project project1 = projectRepository.create("P2","D1",user1.getId());
        final Project project2 = projectRepository.create("P1","D2",user2.getId());

        final Task task1 = taskRepostory.create("T3","D1",user1.getId());
        final Task task2 = taskRepostory.create("T2","D2",user2.getId());
        final Task task3 = taskRepostory.create("T1","D3",user1.getId());

        projectTaskService.addTaskToProject(project1.getId(),task1.getId());
        projectTaskService.addTaskToProject(project2.getId(),task2.getId());
    }

}
