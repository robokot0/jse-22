package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Сервис задач
 */
public class TaskService extends AbstractService{
    /**
     * Приватный конструктор по умолчанию
     */
    private TaskService(){
        super(CommandHistoryService.getInstance());
        this.taskRepostory = TaskRepostory.getInstance();
    }
    /**
     * Единственный экземпляр объекта TaskService
     */
    private static TaskService instance = null;

    /**
     * Получить единственный экземпляр объекта TaskService
     * @return единственный экземпляр объекта TaskService
     */
    public static TaskService getInstance(){
        if (instance == null){
            instance = new TaskService();
        }
        return instance;
    }

    /**
     * Сервис задач в проекте
     */
    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();
    /**
     * Изменить задачу
     * @param task задача
     * @return 0 выполнено
     */
    public int updateTask(final Task task) throws WrongArgumentException {

        final String name = EnterStringCommandParameter("task name");
        final String description = EnterStringCommandParameter("task description");
        update(task.getId(),name,description,task.getUserId());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить задачу по индексу
     * @return 0 выполнено
     */
    public int updateTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[UPDATE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = this.getUser().isAdmin() ?
                findByIndex(index,true):findByIndex(index,this.getUser().getId(),true);
        updateTask(task);
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByName() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("task name");
        final Task task = this.getUser().isAdmin() ?
                removeByName(name):removeByName(name,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по ID
     * @return 0 выполнено
     */
    public int removeTaskByID() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        if (!this.testAuthUser())return 0;
        final long id = EnterLongCommandParameter("task ID");
        final Task task = this.getUser().isAdmin() ?
                removeById(id):removeById(id,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = this.getUser().isAdmin() ?
                removeByIndex(index):removeByIndex(index,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Создать задачу
     * @return 0 выполнено
     */
    public int createTask() throws WrongArgumentException {
        System.out.println("[CREATE TASK]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("task name");
        final String description = EnterStringCommandParameter("task description");
        create(name,description,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все задачи
     * @return 0 выполнено
     */
    public int clearTask() throws WrongArgumentException {
        System.out.println("[CLEAR TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().isAdmin()) clear();
        else clear(this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать информацию по задаче
     * @param task задача
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        ShowResult("[OK]");
    }

    /**
     * Показать задачу по индексу
     * @return 0 выполнено
     */
    public int viewTaskByIndex() throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final  Task task = this.getUser().isAdmin() ?
                findByIndex(index,true):findByIndex(index,this.getUser().getId(),true);
        viewTask(task);
        return 0;
    }

    /**
     * Показать список задач
     * @param tasks список
     */
    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName());
            index ++;
        }
    }

    /**
     * Показать список задач
     * @return 0 выполнено
     */
    public int listTask() throws WrongArgumentException {
        System.out.println("[LIST TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().isAdmin()) viewTasks(findAll());
        else viewTasks(findAll(this.getUser().getId()));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Показать список задач по идентфиикатору проекта
     * @return 0 выполнено
     */
    public int listTaskByProjectId() throws WrongArgumentException {
        System.out.println("[LIST TASK BY PROJECT ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = EnterLongCommandParameter("project ID");
        if(this.getUser().isAdmin()) viewTasks(findAllByProjectId(projectId));
        else viewTasks(findAllByProjectId(projectId,this.getUser().getId()));
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Добавить задачу в проект по идентфиикатору
     * @return 0 выполнео
     */
    public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException, WrongArgumentException {
        final long projectId = EnterLongCommandParameter("project ID");
        final long taskId = EnterLongCommandParameter("task ID");
        projectTaskService.addTaskToProject(projectId,taskId);
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить задачу из проекта по идентификатору
     * @return 0 выполнено
     */
    public int removeTaskFromProjectByIds() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final long projectId = EnterLongCommandParameter("project ID");
        final long taskId = EnterLongCommandParameter("task ID");
        if(this.getUser().isAdmin()) projectTaskService.removeTaskFromProject(projectId,taskId);
        else projectTaskService.removeTaskFromProject(projectId,taskId,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserById(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return 0;
        final long taskId = EnterLongCommandParameter("task ID");
        final Task task = findById(taskId,true);
        update(task.getId(),task.getName(),task.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserByIndex(final User user) throws WrongArgumentException, TaskNotFoundException {
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("task index")-1;
        final Task task = findByIndex(index,true);
        update(task.getId(),task.getName(),task.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }


    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;


    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        return taskRepostory.create(name,userId);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name, final String description,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.create(name, description, userId);
    }

    /**
     * Измениь задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.update(id, name, description, userId);
    }

    /**
     * Удалить все задачи
     */
    public void clear() {
        taskRepostory.clear();
    }

    /**
     * Удалить все задачи пользователя
     */
    public void clear(final Long userId ) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        taskRepostory.clear(userId);
    }

    /**
     * Найти по индексу
     * @param index индекс
     * @return задача
     */
    public Task findByIndex(final Integer index,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        final Task task = taskRepostory.findByIndex(index);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("индекс",index.toString());
        return task;
    }

    /**
     * мНайти по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final Integer index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.findByIndex(index,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("индекс",userId,index.toString());
        return task;
    }

    /**
     * Найти по имени
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException{
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("имя",name);
        return task;
    }

    /**
     * Найти по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("имя",userId,name);
        return task;
    }

    /**
     * Найти по идентификатору
     * @param id идентификатор
     * @return задча
     */
    public Task findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",id.toString());
        return task;
    }

    /**
     * Найти по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задча
     */
    public Task findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException  {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",userId,id.toString());
        return task;
    }

    /**
     * Удалить по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final Integer index) throws WrongArgumentException, TaskNotFoundException {
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeByIndex(index);
        if(task==null) throw new TaskNotFoundException("индекс",index.toString());
        return task;
    }

    /**
     * Удалить по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final Integer index,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.removeByIndex(index,userId);
        if(task==null) throw new TaskNotFoundException("индекс",userId,index.toString());
        return task;
    }

    /**
     * Удалить по идентификаотру
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id) throws WrongArgumentException, TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id);
        if(task==null) throw new TaskNotFoundException("ИД",id.toString());
        return task;
    }

    /**
     * Удалить по идентификаотру и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id,userId);
        if(task==null) throw new TaskNotFoundException("ИД",userId,id.toString());
        return task;
    }

    /**
     * Удалить по имени
     * @param name имя
     * @return задача
     */
    public Task removeByName(final String name) throws WrongArgumentException, TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name);
        if(task==null) throw new TaskNotFoundException("имя",name);
        return task;
    }

    /**
     * Удалить по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByName(final String name,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name,userId);
        if(task==null) throw new TaskNotFoundException("имя",userId,name);
        return task;
    }

    /**
     * Получить все задачи проекта
     * @param projectId
     * @return список задач
     */
    public List<Task> findAllByProjectId(Long projectId) throws WrongArgumentException {
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить все задачи проекта с учетом пользователя
     * @param projectId
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Получить все задачи
     * @return список задач
     */
    public List<Task> findAll() {
        return taskRepostory.findAll();
    }

    /**
     * Получить все задачи пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return taskRepostory.findAll(userId);
    }

    /**
     * Получить задачу по проекту и ид
     * @param projectId ид проекта
     * @param id ид
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",id.toString(),projectId);
        return task;
    }

    /**
     * Получить задачу по проекту ид и пользователю.
     * @param projectId ид проекта
     * @param id ид
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id, final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id, userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("ИД",userId,id.toString(),projectId);
        return task;
    }
    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("----Task commands:");
        logger.info("task-create - Create new task by name. (tcr)");
        logger.info("task-clear - Remove all tasks. (tcl)");
        logger.info("task-list - Display list of tasks. (tl)");
        logger.info("task-view - View task info. (tv)");
        logger.info("task-remove-by-id - Remove task by id. (trid)");
        logger.info("task-remove-by-name - Remove task by name. (trn)");
        logger.info("task-remove-by-index - Remove task by index. (trin)");
        logger.info("task-update-by-index - Update name and description of task by index. (tuin)");
        logger.info("----Task in project commands:");
        logger.info("task-list-by-project-id - Display task list by project if. (tlp)");
        logger.info("task-add-to-project-by-ids - Add task to project by id. (tap)");
        logger.info("task-remove-from-project-by-ids - Remove task from project by id. (trp)");
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsJson() throws IOException {
        taskRepostory.saveAsJson();
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsXML() throws IOException {
        taskRepostory.saveAsXML();
        ShowResult("[OK]");
        return 0;
    }

}
