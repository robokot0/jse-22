package com.nlmkit.korshunov_am.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.entity.Project;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Сервис проектов
 */
public class ProjectService extends AbstractService {
    /**
     * Приватный конструктор по умолчанию
     */
    private ProjectService(){
        super(CommandHistoryService.getInstance());
        this.projectRepository = ProjectRepository.getInstance();
    }
    /**
     * Единственный экземпляр объекта ProjectService
     */
    private static ProjectService instance = null;

    /**
     * Получить единственный экземпляр объекта ProjectService
     * @return единственный экземпляр объекта ProjectService
     */
    public static ProjectService getInstance(){
        if (instance == null){
            instance = new ProjectService();
        }
        return instance;
    }
    /**
     * Логгер
     */
    final Logger logger = LogManager.getLogger(ProjectService.class.getName());
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository;


    /**
     * Создать проект
     * @return 0 создано
     */
    public int createProject() throws WrongArgumentException {
        logger.trace("createProject()");
        logger.info("[CREATE PROJECT]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("project name");
        final String description = EnterStringCommandParameter("project description");
        this.create(name,description,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Изменить проект
     * @param project Проект
     * @return 0 изменено
     */
    public  int updateProject(final Project project) throws WrongArgumentException{
        logger.trace("updateProject({})",project);
        final String name = EnterStringCommandParameter("project name");
        final String description = EnterStringCommandParameter("project description");
        this.update(project.getId(),name,description,project.getUserId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Обновить проект по индексу
     * @return 0 обновлено
     */
    public int updateProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("updateProjectByIndex()");
        logger.info("[UPDATE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index")-1;
        final Project project = this.getUser().isAdmin()?
                this.findByIndex(index,true) :this.findByIndex(index,this.getUser().getId(),true);
        updateProject(project);
        return 0;
    }

    /**
     * Удалить проект по имени
     * @return 0 выполнено
     */
    public int removeProjectByName() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByName()");
        logger.info("[REMOVE PROJECT BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("project name");
        final Project project = this.getUser().isAdmin() ?
                this.removeByName(name):this.removeByName(name,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по ID
     * @return 0 выоплнено
     */
    public int removeProjectByID() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByID()");
        logger.info("[REMOVE PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final Long id = EnterLongCommandParameter("project ID");
        final Project project = this.getUser().isAdmin() ?
                this.removeById(id):this.removeById(id,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по индексу
     * @return 0 выполнено
     */
    public int removeProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeProjectByIndex()");
        logger.info("[REMOVE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final Project project = this.getUser().isAdmin() ?
                this.removeByIndex(index):this.removeByIndex(index,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все проекты
     * @return 0 выполнено
     */
    public int clearProject() throws WrongArgumentException {
        logger.trace("clearProject()");
        logger.info("[CLEAR PROJECT]");
        if (!this.testAuthUser())return 0;
        if (this.getUser().isAdmin())  this.clear();
        else this.clear(this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Вывести список проектов
     * @return 0 выполнено
     */
    public int listProject() throws WrongArgumentException {
        logger.trace("listProject()");
        logger.info("[LIST PROJECT]");
        if (!this.testAuthUser())return 0;
        int index = 1;
        for (final Project project:
                this.getUser().isAdmin() ?
                        this.findAll():
                        this.findAll(this.getUser().getId())
        ) {
            logger.info("{}. {}: {}",index , project.getId(), project.getName());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * ВЫвеси информацию по проекту
     * @param project Проект
     */
    public void viewProject(final Project project) {
        logger.trace("viewProject({})",project);
        logger.info("[VIEW PROJET]");
        logger.info("ID: " + project.getId());
        logger.info("NAME: " + project.getName());
        logger.info("DESCRIPTION: " + project.getDescription());
        ShowResult("[OK]");
    }

    /**
     * Вывести информацию по проекту по индексу
     * @return 0 выполнено
     */
    public int viewProjectByIndex() throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("viewProjectByIndex()");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final  Project project = this.getUser().isAdmin() ?
                this.findByIndex(index,true) :
                this.findByIndex(index,this.getUser().getId(),true);
        viewProject(project);
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserById(final User user) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("setProjectUserById({})",user);
        if (!this.testAdminUser())return 0;
        final Long projectId = EnterLongCommandParameter("project ID");
        final Project project = this.findById(projectId,true);
        this.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserByIndex(final User user) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("setProjectUserByIndex({})",user);
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final Project project = this.findByIndex(index,true);
        this.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }



    /**
     * Создать проект
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project create(final String name, final String description,final Long userId) throws WrongArgumentException {
        logger.trace("create({},{},{})",name,description,userId);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.create(name, description,userId);
    }

    /**
     * Изменить проект
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException {
        logger.trace("update({},{},{},{})",id,name,description,userId);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.update(id, name, description,userId);
    }

    /**
     * Удалить все проекты
     */
    public void clear() {
        logger.trace("clear()");
        projectRepository.clear();
    }

    /**
     * Удалить все проекты пользователя
     * @param userId ид пользователя
     */
    public void clear(final Long userId) throws WrongArgumentException {
        logger.trace("clear({})",userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        projectRepository.clear(userId);
    }

    /**
     * Найти проект по индексу
     * @param index индекс
     * @return проект
     */
    public Project findByIndex(final Integer index,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        logger.trace("findByIndex({},{})",index,throwIfNotFound);
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }

    /**
     * Найти проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByIndex(final Integer index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByIndex({},{},{})",index,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }
    /**
     * Найти проект по итмени
     * @param name имя
     * @return проект
     */
    public Project findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByName({},{})",name,throwIfNotFound);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("имя",name);
        return project;
    }

    /**
     * Найти проект по итмени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("findByName({},{},{})",name,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("имя",userId,name);
        return project;
    }

    /**
     * Найти проект про идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException  {
        logger.trace("findById({},{})",id,throwIfNotFound);
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("ИД",id.toString());
        return project;
    }

    /**
     * Найти проект про идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        logger.trace("findById({},{},{})",id,userId,throwIfNotFound);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("ИД",userId,id.toString());
        return project;
    }

    /**
     * Удалитьпроект по индексу
     * @param index индекс
     * @return проект
     */
    public Project removeByIndex(final Integer index) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByIndex({})",index);
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index);
        if(project==null) throw new ProjectNotFoundException("индекс",index.toString());
        return project;
    }

    /**
     * Удалитьпроект по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByIndex(final Integer index,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByIndex({},{})",index,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index == null) throw new WrongArgumentException("Не задан индекс");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index,userId);
        if(project==null) throw new ProjectNotFoundException("индекс",userId,index.toString());
        return project;
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project removeById(final Long id) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeById({})",id);
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id);
        if(project==null) throw new ProjectNotFoundException("ИД",id.toString());
        return project;
    }
    /**
     * Удалить проект по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeById(final Long id,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeById({},{})",id,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id,userId);
        if(project==null) throw new ProjectNotFoundException("ИД",userId,id.toString());
        return project;
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return проект
     */
    public Project removeByName(final String name) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByName({})",name);
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name);
        if(project==null) throw new ProjectNotFoundException("имя",name);
        return project;
    }

    /**
     * Удалить проект по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByName(final String name,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        logger.trace("removeByName({},{})",name,userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name,userId);
        if(project==null) throw new ProjectNotFoundException("ИД",userId,name);
        return project;
    }

    /**
     * Получить все проекты
     * @return список проектов
     */
    public List<Project> findAll() {
        logger.trace("findAll()");
        return projectRepository.findAll();
    }

    /**
     * Получить все проекты пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) throws WrongArgumentException{
        logger.trace("findAll({})",userId);
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.findAll(userId);
    }

    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        logger.info("----Project commands:");
        logger.info("project-create - Create new project by name. (pcr)");
        logger.info("project-clear - Remove all projects. (pcl)");
        logger.info("project-list - Display list of projects. (pl)");
        logger.info("project-view - View project info. (pv)");
        logger.info("project-remove-by-id - Remove project by id. (prid)");
        logger.info("project-remove-by-name - Remove project by name. (prn)");
        logger.info("project-remove-by-index - Remove project by index. (prin)");
        logger.info("project-update-by-index - Update name and description of project by index. (puin)");
        ShowResult("[OK]");
        return 0;
    }

    public int saveAsJson() throws IOException{
        projectRepository.saveAsJson();
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsXML() throws IOException {
        projectRepository.saveAsXML();
        ShowResult("[OK]");
        return 0;
    }
}
