package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

import java.io.IOException;
import java.util.List;

public class UserService extends AbstractService{
    /**
     * Приватный конструктор по умолчанию
     */
    private UserService(){
        super(CommandHistoryService.getInstance());
    }
    /**
     * Единственный экземпляр объекта UserService
     */
    private static UserService instance = null;
    /**
     * Получить единственный экземпляр объекта UserService
     * @return единственный экземпляр объекта UserService
     */
    public static UserService getInstance(){
        if (instance == null){
            instance = new UserService();
        }
        return instance;
    }
    /**
     * Репозитарий пользователей
     */
    private final UserRepository userRepository = UserRepository.getInstance();
    /**
     * Сервис задач
     */
    private final TaskService taskService = TaskService.getInstance();
    /**
     * Сервис процессов
     */
    private final ProjectService projectService = ProjectService.getInstance();

    /**
     * Получить текущего пользователя
     *
     * @param user пользователь
     */
    @Override
    public void setUser(User user) {
        projectService.setUser(user);
        taskService.setUser(user);
        super.setUser(user);
    }

    /**
     * Вычислить хэш строки
     * @param stringtohash строка для хэширования
     * @return хэш строки
     */
    public String getStringHash(String stringtohash) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(stringtohash.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
    /**
     * Аутентификация пользователя
     * @return 0 выполнено
     */
    public int authUser(){
        final String login = EnterStringCommandParameter("user login");
        final User user = findByLogin(login);
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String password = EnterPasswordCommandParameter("user password");
        if (!this.getStringHash(password).equals(user.getPasswordHash())){
            ShowResult("[FAIL]");
            return 0;
        }
        this.setUser(user);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Смена пароля аутентифицировавшегося пользователя
     * @return 0 выполнено
     */
    public int updateAuthUserPassword(){
        this.updateUserPassword(this.getUser());
        return 0;
    }
    /**
     * Показать данные аутентифицированного пользователя
     * @return 0 выполнено
     */
    public int viewAuthUser(){
        this.viewUser(this.getUser());
        return 0;
    }
    /**
     * Изменить данные аутентифицировавшегося пользователя без изменения роли
     * @return 0 выполнено
     */
    public int updateAuthUser(){
        updateUserDataNoRole(this.getUser());
        return 0;
    }
    public int endAuthUserSession(){
        this.setUser(null);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     * @return 0 выполнено0
     */
    public int updateUserDataNoRole(final User user){
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String login = EnterStringCommandParameter("user login");
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        updateData(user.getId(),login,user.getRole(),firstName,secondName,middleName);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     * @return 0 выполнено0
     */
    public int updateUserData(final User user){
        if(user == null){
            ShowResult("[FAIL]");
            return 0;
        }
        final String login = EnterStringCommandParameter("user login");
        final Role role = Role.valueOf(EnterStringCommandParameter("user role"));
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        updateData(user.getId(),login,role,firstName,secondName,middleName);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить пароль пользователя
     * @param user пользователь
     * @return 0 выполнено0
     */
    public int updateUserPassword(final User user){
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String password = EnterPasswordCommandParameter("user password");
        final String passwordConfirmation = EnterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return 0;
        }
        updatePassword(user.getId(),getStringHash(password));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя по login
     * @return 0 выполнено
     */
    public int updateUserDataByLogin(){
        System.out.println("[UPDATE USER DATA BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User user = findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по login
     * @return 0 выполнено
     */
    public int updateUserPasswordByLogin(){
        System.out.println("[UPDATE USER PASSWORD BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User user = findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по id
     * @return 0 выполнено
     */
    public int updateUserDataById() throws WrongArgumentException {
        System.out.println("[UPDATE USER DATA BY ID]");
        final long id = EnterLongCommandParameter("user ID");
        final User user = findById(id);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по id
     * @return 0 выполнено
     */
    public int updateUserPasswordById() throws WrongArgumentException {
        System.out.println("[UPDATE USER PASSWORD BY ID]");
        final long id = EnterLongCommandParameter("user ID");
        final User user = findById(id);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по index
     * @return 0 выполнено
     */
    public int updateUserDataByIndex() throws WrongArgumentException {
        System.out.println("[UPDATE USER DATA BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = findByIndex(index);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по index
     * @return 0 выполнено
     */
    public int updateUserPasswordByIndex() throws WrongArgumentException {
        System.out.println("[UPDATE USER PASSWORD BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = findByIndex(index);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Удалить пользователя по login
     * @return 0 выполнено
     */
    public int removeUserByLogin(){
        System.out.println("[REMOVE USER BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User userfinded = findByLogin(login);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = removeById(userfinded.getId());
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по id
     * @return 0 выполнено
     */
    public int removeUserById() throws WrongArgumentException {
        System.out.println("[REMOVE USER BY ID]");
        if (!this.testAdminUser())return 0;
        final long id = EnterLongCommandParameter("user ID");
        final User userdeleted = removeById(id);
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по index
     * @return 0 выполнено
     */
    public int removeUserByIndex() throws WrongArgumentException {
        System.out.println("[REMOVE USER BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User userfinded = findByIndex(index);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = removeById(userfinded.getId());
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Создать пользователя
     * @return 0 выполнено
     */
    public int createUser(){
        System.out.println("[CREATE USER]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final Role role = Role.valueOf(EnterStringCommandParameter("user role"));
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        final String password = EnterPasswordCommandParameter("user password");
        final String passwordConfirmation = EnterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return 0;
        }
        create(login,role,firstName,secondName,middleName,getStringHash(password));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать информацию по пользователю
     * @param user пользователь
     */
    public void viewUser(final User user) {
        if (user == null){
            ShowResult("[FAIL]");
            return;
        }
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().toString());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("PASSWORD HASH: " + user.getPasswordHash());
        ShowResult("[OK]");
    }
    /**
     * Показать пользователя по login
     * @return 0 выполнено
     */
    public int viewUserByLogin() {
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = findByLogin(login);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по id
     * @return 0 выполнено
     */
    public int viewUserById() throws WrongArgumentException {
        if (!this.testAdminUser())return 0;
        final long id = EnterLongCommandParameter("user ID");
        final  User user = findById(id);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по index
     * @return 0 выполнено
     */
    public int viewUserByIndex() throws WrongArgumentException {
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = findByIndex(index);
        viewUser(user);
        return 0;
    }
    /**
     * Показать список пользователей
     * @param users список
     */
    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". " + user.getLogin()+ ": " + user.getRole()+ ": " + user.getFirstName()+ ": " + user.getSecondName()+ ": " + user.getMiddleName());
            index ++;
        }
    }
    /**
     * Показать список пользователей
     * @return 0 выполнено
     */
    public int listUser(){
        System.out.println("[LIST USER]");
        if (!this.testAdminUser())return 0;
        viewUsers(findAll());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @return 0 выполнено
     */
    public int setProjectUserById() throws WrongArgumentException, ProjectNotFoundException {
        System.out.println("[SET PROJECT USER FIND PROJECT BY ID]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = findByLogin(login);
        projectService.setProjectUserById(user);
        return 0;
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @return 0 выполнено
     */
    public int setProjectUserByIndex() throws WrongArgumentException, ProjectNotFoundException {
        System.out.println("[SET PROJECT USER FIND PROJECT BY INDEX]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = findByLogin(login);
        projectService.setProjectUserByIndex(user);
        return 0;
    }
    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @return 0 выполнено
     */
    public int setTaskUserById() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[SET TASK USER FIND TASK BY ID]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = findByLogin(login);
        taskService.setTaskUserById(user);
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @return 0 выполнено
     */
    public int setTaskUserByIndex() throws WrongArgumentException, TaskNotFoundException {
        System.out.println("[SET TASK USER FIND TASK BY INDEX]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = findByLogin(login);
        taskService.setTaskUserByIndex(user);
        return 0;
    }




    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public User create(String login) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        return userRepository.create(login);
    }

    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.create(login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.update(id,login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public User updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!(userRepository.findByLogin(login)==null))
          if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        return userRepository.updateData(id,login,role,firstName,secondName,middleName);
    }
    /**
     * Изменить пароль пользователя
     * @param id идентификатор
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User updatePassword(final Long id, String passwordHash) {
        if (id == null) return null;
        if (userRepository.findById(id)==null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.updatePasswordHash(id, passwordHash);
    }

    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        userRepository.clear();
    }
    /**
     * Найти пользователя по id.
     * @param id
     * @return пользователь
     */
    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }
    /**
     * Найти пользователя по login.
     * @param login
     * @return пользователь
     */
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }
    /**
     * Найти пользователя по индексу.
     * @param index
     * @return пользователь
     */
    public User findByIndex(final int index) {
        return userRepository.findByIndex(index);
    }
    /**
     * Показать справку
     * @return 0 выполнено
     */
    public int displayHelp() {
        System.out.println("----User commands:");
        System.out.println("user-create - Create user. (uc)");
        System.out.println("user-list - View list of users. (ul)");
        System.out.println("user-view-by-id - View user data by id. (uvid)");
        System.out.println("user-view-by-index - View user data by index. (uvi)");
        System.out.println("user-view-by-login - View user data by login. (uvl)");
        System.out.println("user-remove-by-id - Remove user by id. (urid)");
        System.out.println("user-remove-by-index - Remove user by index. (uri)");
        System.out.println("user-remove-by-login - Remove user by login. (url)");
        System.out.println("user-update-by-id - Update user data by id. (uuid)");
        System.out.println("user-update-by-index - Update user data by index. (uui)");
        System.out.println("user-update-by-login - Update user data by login. (uul)");
        System.out.println("user-update-password-by-id - Change password of user by id. (uupid)");
        System.out.println("user-update-password-by-index - Change password of user by index. (uupi)");
        System.out.println("user-update-password-by-login - Change password of user by login. (uupl)");
        System.out.println("user-auth - Auth user. (ua)");
        System.out.println("user-update-password - Update password of auth user. (uup)");
        System.out.println("user-view - View auth user data. (uv)");
        System.out.println("user-update - Update auth user data. (uu)");
        System.out.println("user-end-session - End auth user session. (ue)");
        System.out.println("user-of-project-set-by-index - Set user of project. Project find by index (up)");
        System.out.println("user-of-task-set-by-index - Set user of task. Task find by index. (ut)");
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по id
     * @param id
     * @return login
     */
    public User removeById(final Long id) {
        if (id == null) return null;
        if (userRepository.findById(id)==null) return null;
        return userRepository.removeById(id);
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public int saveAsJSON() throws IOException {
        userRepository.saveAsJSON();
        ShowResult("[OK]");
        return 0;
    }
    public int saveAsXML() throws IOException {
        userRepository.saveAsXML();
        ShowResult("[OK]");
        return 0;
    }
}
