package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Command;
import com.nlmkit.korshunov_am.tm.repository.CommandHistoryRepository;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;

import java.util.List;

public class CommandHistoryService extends AbstractService {
    /**
     * Приватный конструктор по умолчанию
     */
    private CommandHistoryService(){
        super(null);
        this.commandHistoryService=this;
    }
    /**
     * Единственный экземпляр объекта CommandHistoryService
     */
    private static CommandHistoryService instance = null;

    /**
     * Получить единственный экземпляр объекта CommandHistoryService
     * @return единственный экземпляр объекта CommandHistoryService
     */
    public static CommandHistoryService getInstance(){
        if (instance == null){
            instance = new CommandHistoryService();
        }
        return instance;
    }

    public int listCommandHistory(){
        System.out.println("[LIST COMMAND HISTORY]");
        int index = 1;
        for (final Command command: this.findAll())
        {
            System.out.println(index + ". " + command.getCommandText());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }


    private final CommandHistoryRepository сommandHistoryRepository = CommandHistoryRepository.getInstance();

    /**
     * Добавляет комманду в историю
     * @param command комманда
     */
    public void AddCommandToHistory(String command) {
        сommandHistoryRepository.AddCommandToHistory(command);
    }

    /**
     * Добавляет параметр комманды в историю
     * @param commandParameter параметр комманды
     * @param parameterValue значение параметра
     */
    public void AddCommandParameterToLastCommand(String commandParameter, String parameterValue) {
        сommandHistoryRepository.AddCommandParameterToLastCommand(commandParameter, parameterValue);
    }

    /**
     * Добавляет результат исполнения комманды
     * @param commandResult результат исполнения комманды
     */
    public void AddCommandResultToLastCommand(String commandResult) {
        сommandHistoryRepository.AddCommandResultToLastCommand(commandResult);
    }

    /**
     * Получить список всех комманд из истории
     * @return список комманд
     */
    public List<Command> findAll() {
        return сommandHistoryRepository.findAll();
    }


    /**
     * Показать справку
     */
    public int displayHelp() {
        logger.info("command-history-view - View command history. (chv)");
        ShowResult("[OK]");
        return 0;
    }

}
